package com.husrevbeyazisik.stajproje.adapters;

/**
 * Created by husrev on 1.06.2017.
 */

public class NavItem {
    public String mTitle;
    public String mSubtitle;

    public NavItem(String title, String subtitle) {
        mTitle = title;
        mSubtitle = subtitle;
    }
}
