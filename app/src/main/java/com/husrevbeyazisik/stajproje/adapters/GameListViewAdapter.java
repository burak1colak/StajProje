package com.husrevbeyazisik.stajproje.adapters;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;


import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.husrevbeyazisik.stajproje.Game;
import com.husrevbeyazisik.stajproje.R;
import com.husrevbeyazisik.stajproje.System;
import com.husrevbeyazisik.stajproje.helpers.DownloadPicture;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by husrev on 2.06.2017.
 */

public class GameListViewAdapter extends BaseAdapter {
    private StorageReference gamePictureReference;
    private List<Game> gameList;
    private Context currentContext;


    public GameListViewAdapter(List<Game> gameList, Context currentContext) {
        this.gameList = gameList;
        this.currentContext = currentContext;

        gamePictureReference = FirebaseStorage.getInstance().getReference(System.STORAGE_GAMES);
    }

    @Override
    public int getCount() {
        return gameList.size();
    }

    @Override
    public Object getItem(int position) {
        return gameList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v =View.inflate(currentContext, R.layout.item_game,null);

        TextView gameName = (TextView) v.findViewById(R.id.txtGameName);
        TextView gameCategory = (TextView) v.findViewById(R.id.txtGameCategory);



        gameName.setText(gameList.get(position).getName());
        gameCategory.setText(gameList.get(position).getCategory());


        return v;
    }
}
