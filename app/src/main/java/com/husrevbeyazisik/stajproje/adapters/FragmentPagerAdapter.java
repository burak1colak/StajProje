package com.husrevbeyazisik.stajproje.adapters;


import android.app.Fragment;
import android.app.FragmentManager;



import com.husrevbeyazisik.stajproje.main.Login;
import com.husrevbeyazisik.stajproje.main.Register;

/**
 * Created by husrev on 31.05.2017.
 */

public class FragmentPagerAdapter extends android.support.v13.app.FragmentPagerAdapter {
    private String[] menuTitles;

    public FragmentPagerAdapter(FragmentManager fm,String[] menuTitles ) {
        super(fm);
        this.menuTitles = menuTitles;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return this.menuTitles[position];
    }

    @Override
    public Fragment getItem(int position) {
        switch (position)
        {
            case 0:
                return new Login();

            case 1:
                return new Register();


            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return menuTitles.length;
    }


}
