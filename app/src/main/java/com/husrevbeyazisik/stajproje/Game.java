package com.husrevbeyazisik.stajproje;

import java.io.Serializable;

/**
 * Created by husrev on 2.06.2017.
 */

public class Game implements Serializable {
    private String id;
    private String name;
    private String category;
    private String description;
    private boolean type;
    private String userId;

    public Game() {
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isType() {
        return type;
    }

    public void setType(boolean type) {
        this.type = type;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
