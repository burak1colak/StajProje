package com.husrevbeyazisik.stajproje.helpers;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.husrevbeyazisik.stajproje.User;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

/**
 * Created by husrev on 2.06.2017.
 */

public class DownloadPicture extends AsyncTask<String,Void,Bitmap> {

        ImageView imgPicture;
        ProgressBar pb=null;


        public DownloadPicture(ImageView img,ProgressBar pb) {
                this.imgPicture = img;
                this.pb = pb;
        }

        public DownloadPicture(ImageView img) {
                this.imgPicture = img;
        }


        @Override
        protected void onPreExecute() {
                super.onPreExecute();
        }

        @Override
        protected Bitmap doInBackground(String... urls) {
                String url = urls[0];
                Bitmap bm=null;

                        try {
                                InputStream is = new URL(url).openStream();
                                bm = BitmapFactory.decodeStream(is);
                        } catch (IOException e) {
                                e.printStackTrace();
                        }
                return bm;
        }

        @Override
        protected void onPostExecute(Bitmap bm)
        {
                this.imgPicture.setImageBitmap(RoundPicture.Round(bm));
                this.imgPicture.setVisibility(View.VISIBLE);
                if(pb!=null)this.pb.setVisibility(View.GONE);
        }
}
