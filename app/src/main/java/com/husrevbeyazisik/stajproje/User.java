package com.husrevbeyazisik.stajproje;

import android.app.Activity;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.facebook.AccessToken;

/**
 * Created by husrev on 31.05.2017.
 */

public class User {
    private String id;
    private String mail;
    private String firstName;
    private String lastName;
    private String password;

    //Giriş yapmış şekilde tutabilmek için statik özellikler
    public static String Id;
    public static String Mail;
    public static String FirstName;
    public static String LastName;

    public User(){}

    public User(String id, String mail, String firstName,String lastName,String password) {
        this.id = id;
        this.mail = mail;
        this.firstName = firstName;
        this.lastName = lastName;
        this.password  =  password;

        //statik ögelerede atama yapıyoruz
        this.Id = id;
        this.Mail = mail;
        this.FirstName = firstName;
        this.LastName = lastName;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = this.Id = id;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = this.Mail = mail;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = this.FirstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = this.LastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public static boolean isFacebookLoggedIn() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        return accessToken != null;
    }

    public static boolean isUserLoggedIn(Activity currentActivity) {
        SharedPreferences loggedUser = PreferenceManager.getDefaultSharedPreferences(currentActivity);

        String id = loggedUser.getString(System.ID,"0");

        return id.equals("0") ? false : true;
    }

    public static void UserLogIn(User user,Activity currentActivity)
    {
        SharedPreferences loggedUser = PreferenceManager.getDefaultSharedPreferences(currentActivity);
        SharedPreferences.Editor editor = loggedUser.edit();

        editor.putString(System.ID,user.getId());
        editor.putString(System.MAIL,user.getMail());
        editor.putString(System.FIRST_NAME,user.getFirstName());
        editor.putString(System.LAST_NAME,user.getLastName());

        editor.commit();
    }
}
