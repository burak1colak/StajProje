package com.husrevbeyazisik.stajproje.home;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.login.LoginManager;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.husrevbeyazisik.stajproje.R;
import com.husrevbeyazisik.stajproje.System;
import com.husrevbeyazisik.stajproje.User;
import com.husrevbeyazisik.stajproje.adapters.DrawerListAdapter;
import com.husrevbeyazisik.stajproje.adapters.NavItem;
import com.husrevbeyazisik.stajproje.helpers.DownloadPicture;
import com.husrevbeyazisik.stajproje.helpers.RoundPicture;
import com.husrevbeyazisik.stajproje.home.fragments.AddGame;
import com.husrevbeyazisik.stajproje.home.fragments.Games;
import com.husrevbeyazisik.stajproje.home.fragments.MyGames;

import java.util.ArrayList;

public class HomeActivity extends AppCompatActivity {


    ListView drawerList;
    TextView txtUserName;
    ImageView imgProfilePicture;

    RelativeLayout drawerPane;
    ActionBarDrawerToggle drawerToggle;
    DrawerLayout drawerLayout;
    Toolbar toolbar;
    ArrayList<NavItem> navItems = new ArrayList<NavItem>();
    FloatingActionButton fab;

    FragmentManager fragmentManager;

    StorageReference pPictureReference;

    Uri picturePath;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        if(!User.isFacebookLoggedIn() && !User.isUserLoggedIn(this))
            finish();

        setupToolbar();
        SetupFab();
        setupMainFragment();
        setupDrawerManu();
        setupProfile();
    }



    private void setupToolbar() {

        toolbar = (Toolbar) findViewById(R.id.homeToolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        toolbar.setTitleTextColor(getResources().getColor(R.color.white));
    }


    private void SetupFab() {
        fab = (FloatingActionButton) findViewById(R.id.fabAddGame);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.fragment, new AddGame())
                        .commit();
            }
        });
    }

    private void setupMainFragment() {
        fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.fragment, new MyGames())
                .commit();
        setTitle(getResources().getString(R.string.my_games_title));

    }

    private void setupDrawerManu() {

        //Menü itemleri
        navItems.add(new NavItem(getResources().getString(R.string.my_games_title), ""));
        navItems.add(new NavItem(getResources().getString(R.string.online_games_title), ""));
        navItems.add(new NavItem(getResources().getString(R.string.offline_games_title), ""));


        // DrawerLayout
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);


        drawerPane = (RelativeLayout) findViewById(R.id.drawerPane);
        drawerList = (ListView) findViewById(R.id.navList);
        DrawerListAdapter adapter = new DrawerListAdapter(this, navItems);
        drawerList.setAdapter(adapter);

        // Drawer item click
        drawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectItemFromDrawer(position);
            }
        });

        //toggle
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout,toolbar, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);

                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);

                invalidateOptionsMenu();
            }
        };


        drawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.setDrawerIndicatorEnabled(true);
        drawerToggle.syncState();
    }




    private void setupProfile() {
        txtUserName = (TextView) findViewById(R.id.txtUserName);
        imgProfilePicture = (ImageView) findViewById(R.id.imgProfilePicture);

        txtUserName.setText(User.FirstName+ " " + User.LastName);



        if(User.isFacebookLoggedIn()) {
            new DownloadPicture(imgProfilePicture).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,"https://graph.facebook.com/" + User.Id + "/picture?type=large");
            return;
        }

        downloadProfilePicture();

        //facebook kullanıcısıysa bu eventi eklemiyoruz
        imgProfilePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pPictureReference = FirebaseStorage.getInstance().getReference(System.STORAGE_PROFILE_PICTURES);

                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_PICK);
                startActivityForResult(Intent.createChooser(intent, "Resim seç"), System.PICK_IMAGE_REQUEST);


            }
        });
    }


    private void selectItemFromDrawer(int position) {
        Fragment fragment = null;

        //online ve offline anlamak için
        Bundle bundle = new Bundle();
        Games g;
        switch (position)
       {
           case 0:
               fragment = new MyGames();
               break;
           case 1:
               bundle.putString(System.GAME_TYPE, "online");
               g = new Games();
               g.setArguments(bundle);
               fragment = g;
               break;
           case 2:
               bundle.putString(System.GAME_TYPE, "offline");
               g = new Games();
               g.setArguments(bundle);
               fragment = g;
               break;
       }

        fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.fragment, fragment)
                .commit();

        drawerList.setItemChecked(position, true);
        setTitle(navItems.get(position).mTitle);

        drawerLayout.closeDrawer(drawerPane);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        //oyun ekleme butonu
        switch (item.getItemId()) {

            case R.id.logout:
                logOut();
                break;

        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.home_menu, menu);
        return true;
    }

    private void logOut()
    {

        LoginManager.getInstance().logOut();

        SharedPreferences loggedUser = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = loggedUser.edit();

        editor.remove(System.ID);
        editor.remove(System.MAIL);
        editor.remove(System.FIRST_NAME);
        editor.remove(System.LAST_NAME);

        editor.commit();

        finish();

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == System.PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Toast.makeText(HomeActivity.this, getResources().getString(R.string.picture_uploading), Toast.LENGTH_SHORT).show();
                picturePath = data.getData();

            StorageReference childRef = pPictureReference.child(User.Id+".jpg");

            //upload
            UploadTask uploadTask = childRef.putFile(picturePath);
            uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    Toast.makeText(HomeActivity.this, getResources().getString(R.string.picture_uploaded), Toast.LENGTH_SHORT).show();
                    downloadProfilePicture();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(HomeActivity.this, getResources().getString(R.string.unknown_error), Toast.LENGTH_SHORT).show();
                }
            });

        }
    }


    private void downloadProfilePicture()
    {
        pPictureReference = FirebaseStorage.getInstance().getReference(System.STORAGE_PROFILE_PICTURES);
        try {
            pPictureReference.child(User.Id + ".jpg").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri) {
                    new DownloadPicture(imgProfilePicture).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, uri.toString());
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                }
            });

        } catch (Exception e) {
        }

    }







}

