package com.husrevbeyazisik.stajproje.home.fragments;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Fragment;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.husrevbeyazisik.stajproje.Game;
import com.husrevbeyazisik.stajproje.R;
import com.husrevbeyazisik.stajproje.System;
import com.husrevbeyazisik.stajproje.User;
import com.husrevbeyazisik.stajproje.adapters.GameListViewAdapter;
import com.husrevbeyazisik.stajproje.home.GameDetail;

import java.util.ArrayList;
import java.util.List;


public class Games extends Fragment {

    DatabaseReference gameDatabasereference;

    ListView gameListView;
    GameListViewAdapter gameListViewAdapter;
    List<Game> gameList;

    TextView txtGamesNoGame;

    public Games() {
        // Required empty public constructor
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_games, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        txtGamesNoGame = (TextView) getActivity().findViewById(R.id.txtGamesNoGame);

        Bundle bundle = getArguments();
        Boolean gameType = bundle.getString(System.GAME_TYPE).equals("online") ? true:false;



        gameDatabasereference = FirebaseDatabase.getInstance().getReference(System.TABLE_GAMES);

        gameListView = (ListView) getActivity().findViewById(R.id.lstGames);





        final Query query = gameDatabasereference.orderByChild(System.TYPE).equalTo(gameType);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                gameList = new ArrayList<Game>();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Game game = snapshot.getValue(Game.class);
                    gameList.add(game);
                }


                if(gameList.size() == 0)
                    txtGamesNoGame.setVisibility(View.VISIBLE);
                else
                    txtGamesNoGame.setVisibility(View.GONE);

                gameListViewAdapter = new GameListViewAdapter(gameList,getActivity());
                gameListView.setAdapter(gameListViewAdapter);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



        gameListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent gameDetail = new Intent(getActivity(), GameDetail.class);
                gameDetail.putExtra("game",gameList.get(position));
                startActivity(gameDetail);
            }
        });

    }
}
