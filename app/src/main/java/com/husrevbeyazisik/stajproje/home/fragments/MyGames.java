package com.husrevbeyazisik.stajproje.home.fragments;


import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.app.Fragment;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.husrevbeyazisik.stajproje.Game;
import com.husrevbeyazisik.stajproje.R;
import com.husrevbeyazisik.stajproje.System;
import com.husrevbeyazisik.stajproje.User;
import com.husrevbeyazisik.stajproje.adapters.GameListViewAdapter;
import com.husrevbeyazisik.stajproje.home.GameDetail;

import java.util.ArrayList;
import java.util.List;


public class MyGames extends Fragment {

    DatabaseReference gameDatabasereference;

    ListView gameListView;
    GameListViewAdapter gameListViewAdapter;
    List<Game> gameList;

    TextView txtMyGamesNoGame;

    public MyGames() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_my_games, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        txtMyGamesNoGame = (TextView) getActivity().findViewById(R.id.txtMyGamesNoGame);

        gameDatabasereference = FirebaseDatabase.getInstance().getReference(System.TABLE_GAMES);

        gameListView = (ListView) getActivity().findViewById(R.id.lstMyGames);


        final Query query = gameDatabasereference.orderByChild(System.USER_ID).equalTo(User.Id);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                gameList = new ArrayList<Game>();
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        Game game = snapshot.getValue(Game.class);
                        gameList.add(game);
                    }

                if(gameList.size() == 0)
                    txtMyGamesNoGame.setVisibility(View.VISIBLE);
                else
                    txtMyGamesNoGame.setVisibility(View.GONE);


                        gameListViewAdapter = new GameListViewAdapter(gameList,getActivity());
                        gameListView.setAdapter(gameListViewAdapter);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



gameListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent gameDetail = new Intent(getActivity(), GameDetail.class);
        gameDetail.putExtra("game",gameList.get(position));
        startActivity(gameDetail);
    }
});



    }
}
