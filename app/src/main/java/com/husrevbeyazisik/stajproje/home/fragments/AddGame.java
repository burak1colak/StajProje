package com.husrevbeyazisik.stajproje.home.fragments;


import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.husrevbeyazisik.stajproje.Game;
import com.husrevbeyazisik.stajproje.R;
import com.husrevbeyazisik.stajproje.System;
import com.husrevbeyazisik.stajproje.User;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddGame extends Fragment {

    DatabaseReference gameDatabaseReference;
    StorageReference gamePictureReference;

    EditText edtGameName,edtAddGameDescription;
    Spinner spnGameCategory;
    CheckBox chkGameType;
    Button btnAddGame,btnAddGamePicture;
    ProgressDialog progressDialog;

    Uri picturePath;


    public AddGame() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add_game, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        edtGameName = (EditText) getActivity().findViewById(R.id.edtAddGameName);
        edtAddGameDescription = (EditText) getActivity().findViewById(R.id.edtAddGameDescription);
        spnGameCategory = (Spinner) getActivity().findViewById(R.id.spnAddGameCategory);
        chkGameType = (CheckBox) getActivity().findViewById(R.id.chkAddGameType);
        btnAddGamePicture = (Button) getActivity().findViewById(R.id.btnAddGamePicture);
        btnAddGame = (Button) getActivity().findViewById(R.id.btnAddGame);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle(getResources().getString(R.string.adding));
        progressDialog.setCancelable(false);


        btnAddGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                progressDialog.show();

                if(!System.isConnect(getActivity()))
                {
                    Toast.makeText(getActivity(), getResources().getString(R.string.no_connection), Toast.LENGTH_SHORT).show();
                    return;
                }

                //Veritabanı ve Storage
                gameDatabaseReference = FirebaseDatabase.getInstance().getReference(System.TABLE_GAMES);
                gamePictureReference = FirebaseStorage.getInstance().getReference(System.STORAGE_GAMES);

                String gameName = edtGameName.getText().toString();
                String description = edtAddGameDescription.getText().toString();
                String category = String.valueOf(spnGameCategory.getSelectedItem());
                Boolean online = chkGameType.isChecked();

                if(TextUtils.isEmpty(gameName))
                {
                    edtGameName.setError(getResources().getString(R.string.game_name_empty));
                    progressDialog.dismiss();
                    return;
                }

                if(TextUtils.isEmpty(description))
                {
                    edtAddGameDescription.setError(getResources().getString(R.string.add_game_description));
                    progressDialog.dismiss();
                    return;
                }

                if(picturePath == null) {
                    btnAddGamePicture.setError(getResources().getString(R.string.select_game_picture));
                    progressDialog.dismiss();
                    return;
                }

                Game game = new Game();
                game.setId(gameDatabaseReference.push().getKey());
                game.setName(gameName);
                game.setCategory(category);
                game.setDescription(description);
                game.setType(online);
                game.setUserId(User.Id);





                    StorageReference childRef = gamePictureReference.child(game.getId()+".jpg");

                    //upload
                    UploadTask uploadTask = childRef.putFile(picturePath);
                    uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            progressDialog.dismiss();
                            Toast.makeText(getActivity(), getResources().getString(R.string.game_added), Toast.LENGTH_SHORT).show();
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();
                            Toast.makeText(getActivity(), getResources().getString(R.string.unknown_error), Toast.LENGTH_SHORT).show();
                        }
                    });



                gameDatabaseReference.child(game.getId()).setValue(game);


            }
        });



        btnAddGamePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_PICK);
                startActivityForResult(Intent.createChooser(intent, getResources().getString(R.string.choose_picture)), System.PICK_IMAGE_REQUEST);

            }
        });

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == System.PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            picturePath = data.getData();
            btnAddGamePicture.setText(getResources().getString(R.string.choosed));
        }
    }
}
