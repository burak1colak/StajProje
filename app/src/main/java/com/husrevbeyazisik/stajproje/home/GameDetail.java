package com.husrevbeyazisik.stajproje.home;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.husrevbeyazisik.stajproje.Game;
import com.husrevbeyazisik.stajproje.R;
import com.husrevbeyazisik.stajproje.System;
import com.husrevbeyazisik.stajproje.User;
import com.husrevbeyazisik.stajproje.helpers.DownloadPicture;

public class GameDetail extends AppCompatActivity {

    TextView txtGameName,txtGameCategory,txtGameType,txtGameDescription,txtGameUserName;
    ImageView imgGamePicture;
    ProgressBar pbGameImgLoading;
    Button btnShareOnTwitter;

    DatabaseReference databaseReference;
    StorageReference gamePictureReference;

    Game game;
    User user;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_detail);




        databaseReference = FirebaseDatabase.getInstance().getReference();
        gamePictureReference = FirebaseStorage.getInstance().getReference(System.STORAGE_GAMES);

        txtGameName = (TextView) findViewById(R.id.txtGameName);
        txtGameCategory = (TextView) findViewById(R.id.txtGameCategory);
        txtGameType = (TextView) findViewById(R.id.txtGameType);
        txtGameDescription = (TextView) findViewById(R.id.txtGameDescription);
        txtGameUserName = (TextView) findViewById(R.id.txtGameUserName);
        imgGamePicture = (ImageView) findViewById(R.id.imgGamePicture);
        pbGameImgLoading = (ProgressBar) findViewById(R.id.pbGameImgLoading);
        btnShareOnTwitter = (Button) findViewById(R.id.btnShare);


        //yüklenene kadar gizlemek için
        imgGamePicture.setVisibility(View.GONE);

        game = (Game)getIntent().getSerializableExtra("game");

        txtGameName.setText(game.getName());
        txtGameCategory.setText(game.getCategory());
        txtGameType.setText(game.isType()?"online":"offline");
        txtGameDescription.setText(game.getDescription());

        final Query query = databaseReference.child(System.TABLE_USERS).orderByChild(System.ID).equalTo(game.getUserId());
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists())
                {
                    user = dataSnapshot.child(game.getUserId()).getValue(User.class);
                    txtGameUserName.setText(user.getFirstName() +" " + user.getLastName() + " tarafından eklendi");
                    return;
                }


                //iş bitti listener siliniyor
                query.removeEventListener(this);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(GameDetail.this, getResources().getString(R.string.unknown_error), Toast.LENGTH_SHORT).show();

            }
        });


        try {
            gamePictureReference.child(game.getId()+".jpg").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri) {
                    new DownloadPicture(imgGamePicture, pbGameImgLoading).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,uri.toString());
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                }
            });

        }catch (Exception e)
        {
        }

        btnShareOnTwitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Share();
            }
        });




    }


    private void Share()
    {
            Intent share = new Intent(Intent.ACTION_SEND);
            share.setType("text/plain");
            share.putExtra(Intent.EXTRA_TEXT, game.getName()+": "+game.getDescription());

            startActivity(Intent.createChooser(share, getResources().getString(R.string.share_title)));
    }
}
