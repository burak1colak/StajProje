package com.husrevbeyazisik.stajproje;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by husrev on 31.05.2017.
 */

public abstract class System {


    public static final String TABLE_USERS = "Users";
    public static final String TABLE_GAMES = "Games";
    public static final String STORAGE_GAMES = "gamePicture";
    public static final String STORAGE_PROFILE_PICTURES = "pp";

    //facebook
    public static final String ID = "id";
    public static final String EMAIL = "email";
    public static final String NAME = "name";


    public static final String USER_ID = "userId";
    public static final String MAIL = "mail";
    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "lastName";
    public static final String PROFILE_PICTURE = "profilePicture";



    public static final String GAME_TYPE = "game_type";
    public static final String TYPE = "type";
    //public static final String SELF_GAMES = "self_games";


    public static final int PICK_IMAGE_REQUEST = 111;
    public static final int UPLOAD_PICTURE = 112;


    public static boolean isConnect(Activity a)
    {
        Activity currentActivity = a;
        ConnectivityManager connectivityManager  = (ConnectivityManager) a.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}
