package com.husrevbeyazisik.stajproje.main;
import android.app.Fragment;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import com.facebook.FacebookSdk;
import com.husrevbeyazisik.stajproje.R;

public class MainActivity extends FragmentActivity {
    FragmentPagerAdapter fragmentPagerAdapter;
    ViewPager viewPager;
    TabLayout mainTabLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_main);


        viewPager = (ViewPager) findViewById(R.id.MainViewPager);
        mainTabLayout = (TabLayout) findViewById(R.id.MainTabLayout);

        //Fragmentler ayarlanıyor
        fragmentPagerAdapter = new com.husrevbeyazisik.stajproje.adapters.FragmentPagerAdapter(getFragmentManager(), getResources().getStringArray(R.array.MainTabTitles));
        viewPager.setAdapter(fragmentPagerAdapter);
        mainTabLayout.setupWithViewPager(viewPager);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //fragmente yönlendirme
        Fragment fragment = getFragmentManager().findFragmentByTag("android:switcher:" + R.id.MainViewPager + ":" + "0");
        fragment.onActivityResult(requestCode, resultCode, data);
    }
}