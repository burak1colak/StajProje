package com.husrevbeyazisik.stajproje.main;


import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.husrevbeyazisik.stajproje.R;
import com.husrevbeyazisik.stajproje.System;
import com.husrevbeyazisik.stajproje.User;
import com.husrevbeyazisik.stajproje.home.HomeActivity;


public class Register extends Fragment {

    EditText edtMail, edtFirstName, edtLastName, edtPassword, edtPasswordConfirm;
    Button btnRegister;
    DatabaseReference userDatabaseReference;
    User user;
    ProgressDialog pb;

    public Register() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_register, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pb = new ProgressDialog(getActivity());
        pb.setCancelable(false);
        pb.setMessage(getResources().getString(R.string.please_wait));

        userDatabaseReference = FirebaseDatabase.getInstance().getReference(System.TABLE_USERS);

        edtMail = (EditText) getActivity().findViewById(R.id.edtRegisterMail);
        edtFirstName = (EditText) getActivity().findViewById(R.id.edtRegisterFirstName);
        edtLastName = (EditText) getActivity().findViewById(R.id.edtRegisterLastName);
        edtPassword = (EditText) getActivity().findViewById(R.id.edtRegisterPassword);
        edtPasswordConfirm = (EditText) getActivity().findViewById(R.id.edtRegisterPasswordConfirm);
        btnRegister = (Button) getActivity().findViewById(R.id.btnRegister);


        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!System.isConnect(getActivity()))
                {
                    Toast.makeText(getActivity(), getResources().getString(R.string.no_connection), Toast.LENGTH_SHORT).show();
                    return;
                }

                String id;
                String mail = edtMail.getText().toString();
                String firstName = edtFirstName.getText().toString();
                String lastName = edtLastName.getText().toString();
                String password = edtPassword.getText().toString();
                String passwordConfirm = edtPasswordConfirm.getText().toString();



                //Alan boş kontrolleri
                if(TextUtils.isEmpty(mail))
                {
                    edtMail.setError(getResources().getString(R.string.not_empty));
                    return;
                }
                if(TextUtils.isEmpty(firstName))
                {
                    edtFirstName.setError(getResources().getString(R.string.not_empty));
                    return;
                }
                if(TextUtils.isEmpty(lastName))
                {
                    edtLastName.setError(getResources().getString(R.string.not_empty));
                    return;
                }
                if(TextUtils.isEmpty(password))
                {
                    edtPassword.setError(getResources().getString(R.string.not_empty));
                    return;
                }
                if(TextUtils.isEmpty(passwordConfirm))
                {
                    edtPasswordConfirm.setError(getResources().getString(R.string.not_empty));
                    return;
                }

                //mail geçerlilik
                if(!android.util.Patterns.EMAIL_ADDRESS.matcher(mail).matches())
                {
                    edtMail.setError(getResources().getString(R.string.invalid_mail));
                    return;
                }


                //şifre eşleşme
                if(!password.equals(passwordConfirm))
                {
                    edtPasswordConfirm.setError(getResources().getString(R.string.password_not_match));
                    return;
                }


                id = userDatabaseReference.push().getKey();
                user = new User(id,mail,firstName,lastName,password);

                RegisterUser();

            }
        });

    }

    private void RegisterUser() {
        pb.show();
        final Query query = userDatabaseReference.orderByChild(System.MAIL).equalTo(user.getMail());
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists())
                {
                    pb.dismiss();
                    edtMail.setError(getResources().getString(R.string.mail_exists));
                    return;
                }

                userDatabaseReference.child(user.getId()).setValue(user);

                Intent homeActivity = new Intent(getActivity(), HomeActivity.class);
                getActivity().startActivity(homeActivity);

                User.UserLogIn(user,getActivity());
                pb.dismiss();
                StartHomeActivity();

                //iş bitti listener siliniyor
                query.removeEventListener(this);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(getActivity(), getResources().getString(R.string.unknown_error), Toast.LENGTH_SHORT).show();
                pb.dismiss();
            }
        });

    }


    private void StartHomeActivity() {
        Intent homeActivity = new Intent(getActivity(), HomeActivity.class);
        getActivity().startActivity(homeActivity);
    }


}
