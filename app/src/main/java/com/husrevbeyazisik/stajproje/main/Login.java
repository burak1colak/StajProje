package com.husrevbeyazisik.stajproje.main;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.husrevbeyazisik.stajproje.R;
import com.husrevbeyazisik.stajproje.System;
import com.husrevbeyazisik.stajproje.User;
import com.husrevbeyazisik.stajproje.home.HomeActivity;

import org.json.JSONException;
import org.json.JSONObject;


import java.util.Arrays;

public class Login extends Fragment {

    EditText edtMail, edtPassword;
    Button btnLogin;
    LoginButton fbLoginButton;
    CallbackManager callbackManager;
    User user;
    DatabaseReference userDatabaseReference;
    ProgressDialog pb;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return  inflater.inflate(R.layout.fragment_login, container, false);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        pb = new ProgressDialog(getActivity());
        pb.setCancelable(false);
        pb.setMessage("Lütfen bekleyin...");
        pb.show();


        userDatabaseReference = FirebaseDatabase.getInstance().getReference(System.TABLE_USERS);

        fbLoginButton = (LoginButton) getActivity().findViewById(R.id.fb_login_button);
        edtMail = (EditText) getActivity().findViewById(R.id.edtLoginMail);
        edtPassword = (EditText) getActivity().findViewById(R.id.edtLoginPassword);
        btnLogin = (Button) getActivity().findViewById(R.id.btnLogin);

        //kullanıcı giriş yaptıysa yönlendiriyoruz
        loggedControl();
        facebookLogin();
        userLogin();


    }


    private void loggedControl() {
        if(User.isFacebookLoggedIn()) {
            getFacebookUser(AccessToken.getCurrentAccessToken());
            return;
        }
        else if(User.isUserLoggedIn(getActivity()))
        {
            SharedPreferences loggedUser = PreferenceManager.getDefaultSharedPreferences(getActivity());

            //kullanıcı bilgilerini alıp statik ögelere ata
            new User(loggedUser.getString(System.ID,"0"),
                    loggedUser.getString(System.MAIL,"0"),
                    loggedUser.getString(System.FIRST_NAME,"0"),
                    loggedUser.getString(System.LAST_NAME,"0"),
                    null);

            pb.dismiss();
            StartHomeActivity();
        }

        pb.dismiss();
    }


    private void facebookLogin() {

        callbackManager = CallbackManager.Factory.create();

        fbLoginButton.setReadPermissions(Arrays.asList("public_profile", "email")); //permissions

        fbLoginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                getFacebookUser(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                Toast.makeText(getActivity(), getResources().getString(R.string.cancel), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(getActivity(), getResources().getString(R.string.unknown_error), Toast.LENGTH_SHORT).show();
                Log.i("login","registerCallBack error: "+ error.toString());
            }
        });
    }

    private void userLogin() {
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!System.isConnect(getActivity()))
                {
                    Toast.makeText(getActivity(), getResources().getString(R.string.no_connection), Toast.LENGTH_SHORT).show();
                    return;
                }


                String mail = edtMail.getText().toString().trim();
                final String password= edtPassword.getText().toString().trim();

                if(TextUtils.isEmpty(mail))
                {
                    edtMail.setError(getResources().getString(R.string.not_empty));
                    return;
                }
                if(TextUtils.isEmpty(password))
                {
                    edtPassword.setError(getResources().getString(R.string.not_empty));
                    return;
                }

                pb.show();
                //Mail kontrolü
                final Query query = userDatabaseReference.orderByChild(System.MAIL).equalTo(mail);
                query.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if(!dataSnapshot.exists())
                        {
                            Toast.makeText(getActivity(),getResources().getString(R.string.user_not_exists), Toast.LENGTH_SHORT).show();
                            pb.dismiss();
                        }else
                        {
                            try{

                                user = dataSnapshot.getChildren().iterator().next().getValue(User.class);

                                //facebook account
                                if(user.getPassword()==null)
                                {
                                    pb.dismiss();
                                    Toast.makeText(getActivity(), getResources().getString(R.string.login_with_facebook_button), Toast.LENGTH_SHORT).show();
                                    return;
                                }

                                //hatalı şifre
                                if(!user.getPassword().equals(password))
                                {
                                    edtPassword.setText("");
                                    edtPassword.setError(getResources().getString(R.string.wrong_password));
                                    pb.dismiss();
                                    return;
                                }

                                User.UserLogIn(user,getActivity());

                                pb.dismiss();
                                Intent homeActivity = new Intent(getActivity(), HomeActivity.class);
                                getActivity().startActivity(homeActivity);

                            }
                            catch (Exception e)
                            {
                                Log.i("login",e.toString());
                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        });

    }




    @Override
    public void onResume() {
        super.onResume();

        loggedControl();
    }







    private void getFacebookUser(AccessToken accessToken)
    {
        if(!System.isConnect(getActivity()))
        {
            Toast.makeText(getActivity(), getResources().getString(R.string.no_connection), Toast.LENGTH_SHORT).show();
            return;
        }

        GraphRequest request = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {

                //Kullanıcı bilgileri alınıyor
                try {
                    String name = object.getString(System.NAME);
                    String[] separatedName = name.split(" ");

                    user = new User(object.getString(System.ID),object.getString(System.EMAIL),separatedName[0],separatedName[separatedName.length -1],null);
                } catch (JSONException e) {
                    Toast.makeText(getActivity(), getResources().getString(R.string.unknown_error), Toast.LENGTH_SHORT).show();
                    Log.i("login","JSONException error: "+ e.toString());
                }


                //Kullanıcı veritabanında yoksa bilgileri ekleniyor
                userDatabaseReference.child(user.getId()).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        //veritabanında yok. ekle
                        if (!dataSnapshot.exists())
                            userDatabaseReference.child(user.getId()).setValue(user);

                        pb.dismiss();
                        //Home Activity ye yönlendir
                        StartHomeActivity();


                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Toast.makeText(getActivity(), getResources().getString(R.string.unknown_error), Toast.LENGTH_SHORT).show();
                        Log.i("login","DatabaseError error: "+ databaseError.toString());
                    }
                });

            }
        });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email");
        request.setParameters(parameters);
        request.executeAsync();

        Log.i("login","succes: user id: "+accessToken.getUserId());
    }


    private void StartHomeActivity() {
        Intent homeActivity = new Intent(getActivity(), HomeActivity.class);
        getActivity().startActivity(homeActivity);
    }



    //MainActivitye giden onActivityResult buraya yönlendirilip callbakManager a yönlendiriliyor
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode,resultCode,data);
    }

}
